var ActiveCampaign = require("activecampaign");
//var KEY = "59a56e949e771e3f1d8d6a51646487f1e76149d1c3f7aa314a2b6d0845b004a9a1765a40";
//var ac = new ActiveCampaign("https://magico.api-us1.com", KEY);
//var KEY = "c150c9c24fb3eb30dca32f6487d03c8144dbf9c63bffbfe26e69a220b4bd52c13e0a4c28";
//var ac = new ActiveCampaign("pippone.activehosted.com", KEY);
var request = require('request');
var KEY = "20d5c900274df50dc70fd6cc69908176be7c6ccd144671a36c22077ab06379bd1141fb15";
var ac = new ActiveCampaign("https://traiplersrl.api-us1.com", KEY);

var _ = require("lodash");
var Q = require("q");
var DEAL_AGILE = require("./json/DEAL_AGILE.json");
var AUTOMATIONS_SCORES = require("./json/AUTOMATIONS_SCORES.json");

// TEST API credentials
ac.credentials_test().then(function(result) {
    // successful request
    if (result.success) {
        // VALID ACCOUNT
        console.info("OK")
    } else {
        // INVALID ACCOUNT
    }
    //console.log(result)
}, function(result) {
    // request error
});

// GET requests

/*var account_view = ac.api("account/view", {});
account_view.then(function(result) {
    // successful request
    console.log(result);
}, function(result) {
    // request error
});*/

function check_contact(email) {
    var contact_exists = ac.api("contact/view?email=" + email, {});
    return contact_exists.then(function(result) {
        // successful request
        var exist = result.success;
        console.log("ESISTE ? ", exist);
        return exist;

    }, function(result) {
        // request error
        console.error(result);

    });

}


function create_contact() {
    var data = {
        "email": "gino8080@gmail.com",
        "first_name": "GINOAPI NEW",
        "last_name": "GINOAPI NEW"
    };

    check_contact(data.email).then(function(exist) {
        console.info(exist)
        if (exist) {
            //fai update ?
        } else {
            var request = ac.api("contact/add", data, function(response) {
                console.log(response);
                if (response.success) {
                    console.log(response.success, response)
                } else {
                    // request error
                    console.error(response.error);
                }
            });
        }
    })
}

function add_tags() {
    var data = {
        "email": "gino8080@gmail.com",
        "tags[0]": "ciao",
        "tags[1]": "Lead"
    };

    var request = ac.api("contact/tag/add", data, function(response) {
        console.log(response);
        if (response.success) {
            // successful request
        } else {
            // request error
            console.error(response.error);
        }
    });
}

function create_list() {
    // POST request

    var list = {
        name: "List 3",
        sender_name: "My Company",
        sender_addr1: "123 S. Street",
        sender_city: "Chicago",
        sender_zip: "60601",
        sender_country: "USA"
    };

    var list_add = ac.api("list/add", list);
    list_add.then(function(result) {
        // successful request
        console.log(result);
    }, function(result) {
        // request error
    });

}

/****
DEALS
*****/
var curr_lead = 0;

function insert_agile_deals() {

    //_.forEach(DEAL_AGILE, function(deal, key) {

    var deal = DEAL_AGILE[curr_lead]
    console.log("current deal " + curr_lead, deal);

    if (deal['Related Contacts'] == "") {
        add_deal(deal).then(function(response) {
            console.info(response);
            if (response.success) {
                // successful request
                next_deal();
            } else {
                // request error
                console.log(response.error);
                next_deal();
            }
        })
    } else {
        next_deal();
    }

}

function next_deal() {
    if (curr_lead < DEAL_AGILE.length - 1) {

        curr_lead++;
        //console.log("NEXT LEAD ", curr_lead)
        insert_agile_deals();

    } else {
        console.log("FINITO")
    }

}

function add_deal(deal) {



    /*
    "Deal ID": "ID_4798065068212224",
    "Name": "Agripanetteria La Cascinetta",
    "Description": "",
    "Probability": "0%",
    "Track": "Default",
    "Milestone": "Closed",
    "Value": "€ 0.0",
    "Close Date": "",
    "Owner": "g.carbonara@traipler.com",
    "Related Contacts": "cri.raviola@gmail.com",
    "Deal Source": "",
    "Loss Reason": "",
    "Created Date": "09/02/2016",
    "Won Date": "",
    "Tags": "Lead Acquisito",
    "Tags Time": "09/02/2016 13:58:51",
    */

    var value = parseFloat(deal['Value'].replace(/[^0-9-.]/g, '')); // 12345.99
    var stage = get_pipeline_phase(deal.Milestone).id;

    var NEW_DEAL = {
        "title": deal['Name'],
        "value": value,
        "currency": "eur",
        "pipeline": 1,
        "stage": stage,
        "owner": 1,
        "contact": deal['Related Contacts'],
        "contact_name": deal['Name'],
        //"contact_phone": "",
        "note": deal['Description']
    };

    console.info("NEW DEAL DATA", NEW_DEAL)
    return ac.api("deal/add", NEW_DEAL)

}

function get_pipeline_phase(milestone) {

    var phases = {
        "First Contact": {
            "id": "5",
            "title": "First Contact",
            "color": "32B0FC",
            "order": "2",
            "pipeline": "1"
        },
        "Ongoing": {
            "id": "8",
            "title": "Ongoing",
            "color": "E6CE4B",
            "order": "3",
            "pipeline": "1"
        },
        "Closed": {
            "id": "9",
            "title": "Closed",
            "color": "993DF6",
            "order": "4",
            "pipeline": "1"
        },
        "Lead": {
            "id": "11",
            "title": "Lead",
            "color": "696969",
            "order": "1",
            "pipeline": "1"
        },
        "Lost": {
            "id": "12",
            "title": "Lost",
            "color": "FF6A74",
            "order": "6",
            "pipeline": "1"
        },
        "Won": {
            "id": "13",
            "title": "Won",
            "color": "18D499",
            "order": "5",
            "pipeline": "1"
        }
    }

    return phases[milestone];
}

function get_user(userid) {
    var contact_exists = ac.api("contact/view", {
        id: userid
    });
    return contact_exists.then(function(user) {
        // console.log(JSON.stringify(user))

        update_user(user);

    }, function(result) {
        // request error
        console.error(result);
    });

}

function get_users() {
    var contact_exists = ac.api("contact/list", {

        //'filters[listid]': '1,2', //iscritti newsletter, Whitepapaer
        //'filters[listid]': '6', //Lead = tutti
        ids: "2084,2269,2262,2091",
        full: 1,
        page: 1
    });

    // List ID's associated with contact: exact match. Provide multiple values (performs an OR operation) like this: '4,7'
    //'filters[listid]' => '4',
    // Filter on custom field values (include the custom field ID, or personalization tag surrounded by percent signs)
    //'filters[fields][%PERS_1%]' => 'value1 match',

    // Filter on custom field values (include the custom field ID, or personalization tag surrounded by percent signs)
    //'filters[fields][%PERS_2%]' => 'value2 match',

    // whether or not to return ALL data, or an abbreviated portion (set to 0 for abbreviated)
    //'full' => 1,

    // optional: change how results are sorted (default is below)
    //'sort' => 'id', // possible values: id, datetime, first_name, last_name
    //'sort_direction' => 'DESC', // ASC or DESC
    //'page' => 2, // pagination - results are limited to 20 per page, so specify what page to view (default is 1)

    return contact_exists.then(function(result) {
        console.info("RESULT", result)
        _.forEach(result, function(contact, key) {
            if (contact.email) {
                console.log("TEMPO " + key, contact)
                setTimeout(function() {
                    update_user(contact);
                }, key + 1000)

            }
        })

    }, function(result) {
        // request error
        console.error(result);
    });

}

function get_automations() {
    ac.api("automation/list", {})
        .then(function(result) {

            console.log(JSON.stringify(result))
            _.forEach(result, function(contact, key) {
                console.log(contact)
            })

        }, function(result) {
            // request error
            console.error(result);
        });
}

function calculate_user_score(user) {
    console.log("automation_history", user.automation_history.length)
    var TOTAL_SCORE = 0;
    _.forEach(user.automation_history, function(user_automation, index) {
        _.forEach(AUTOMATIONS_SCORES, function(automation) {
            if (automation.id == user_automation.id) {
                TOTAL_SCORE += parseInt(automation.score, 10);
                console.log("FOUND", TOTAL_SCORE)
            }
        });
    })

    console.log("TOTAL_SCORE", TOTAL_SCORE)

    return TOTAL_SCORE;
}

function update_user(user) {

    var LEAD_CALDO = false;
    var score = calculate_user_score(user);
    if (score >= 80) {

        user.tags.push("Lead Caldo");

        LEAD_CALDO = {
            id: user.id,
            email: user.email,
            first_name: user.first_name,
            last_name : user.last_name,
            tags: user.tags.join(),
            score: score,
            phone: user.phone,
            category: get_user_category(user),
            last_page: get_user_last_page(user)
        }

    }

    var datas = {
        id: user.id,
        email: user.email,
        tags: user.tags.join(),
        "field[%TOTALSCORE%,0]": score
    }

    //aggiungi stesse liste esistenti
    user.listslist.split("-").forEach(function(list_id) {
        datas["p[" + list_id + "]"] = list_id
    });

    console.info("datas user", user.email, datas)

    ac.api("contact/edit", datas)
        .then(function(updated) {
            console.log("AGGIORNATO USER", user, updated)
            if (LEAD_CALDO) {
                call_zapier(LEAD_CALDO);
            }
        }, function(result) {
            // request error
            console.error(result);
        });

}

function get_user_category(user) {
    var category = "";
    if (user.fields && user.fields['4'] && user.fields['4'].val) {
        category = user.fields['4'].val.replace("||","").replace("||","");
    }

    return category;
}


function get_user_last_page(user) {
    var last_page = "";
    last_page_found = _.find(user.actions, function(action) {
        return action.type == "tracking";
    });
    if(last_page_found){
        last_page = last_page_found.text.replace("Pagina visitata - ","");
    }

    return last_page;
}


function call_zapier(LEAD_CALDO){

    console.info(JSON.stringify(LEAD_CALDO))

    var options = {
      uri: 'https://hooks.zapier.com/hooks/catch/1421102/13pzuw/',
      method: 'POST',
      json: LEAD_CALDO
    };

    request(options, function (error, response, body) {
      if (!error && response.statusCode == 200) {
        console.log(body)
      }
    });



}


curr_user = 0;

function cycle_contacts() {

    console.log("curr_user " + curr_user);

    if (curr_user.tags) {
        add_deal(deal).then(function(response) {
            console.info(response);
            if (response.success) {
                // successful request
                next_deal();
            } else {
                // request error
                console.log(response.error);
                next_deal();
            }
        })
    } else {
        next_deal();
    }

}

/*_.forEach(AUTOMATIONS_SCORES, function(automation_score, automation_name) {
    console.log(automation_name,automation_score)

    //aut[automation_score.id] = automation_score
})*/

get_users()
//    get_user(2084)
    //get_automations()
    //insert_agile_deals()
    //add_tags()
    //create_contact()
    //check_contact("gino8080@gmail.com")
